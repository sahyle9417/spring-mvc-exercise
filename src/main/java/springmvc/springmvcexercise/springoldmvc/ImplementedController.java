package springmvc.springmvcexercise.springoldmvc;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component("/spring-old-mvc/implemented-controller")
public class ImplementedController implements Controller {

    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        System.out.println("ImplementedController.handleRequest");
        // resolved to /WEB-INF/views/new-form.jsp using application.properties
        return new ModelAndView("new-form");
    }
}
