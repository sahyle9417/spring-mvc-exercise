package springmvc.springmvcexercise.servlet.request;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

@WebServlet(name = "requestHeaderServlet", urlPatterns = "/request-header")
public class RequestHeaderServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        printRequestHeaderNames(request);
        printRequestHeaderValues(request);
        printRequestHeaderValue(request, "host");
    }

    private void printRequestHeaderNames(HttpServletRequest request) {
        System.out.println("RequestHeaderServlet.printRequestHeaderNames");

//        Enumeration<String> headerNames = request.getHeaderNames();
//        while (headerNames.hasMoreElements()) {
//            String headerName = headerNames.nextElement();
//            System.out.println("headerName = " + headerName);
//        }

        request.getHeaderNames().asIterator()
                .forEachRemaining(headerNames -> System.out.println("headerNames = " + headerNames));

    }

    private void printRequestHeaderValues(HttpServletRequest request) {
        System.out.println("RequestHeaderServlet.printRequestHeaderValues");

        // request.getMethod() = GET
        System.out.println("request.getMethod() = " + request.getMethod());
        // request.getProtocol() = HTTP/1.1
        System.out.println("request.getProtocol() = " + request.getProtocol());
        // request.getScheme() = http
        System.out.println("request.getScheme() = " + request.getScheme());
        // request.getRequestURL() = http://localhost:8080/request-header
        System.out.println("request.getRequestURL() = " + request.getRequestURL());
        // request.getRequestURI() = /request-header
        System.out.println("request.getRequestURI() = " + request.getRequestURI());
        // request.getQueryString() = null
        System.out.println("request.getQueryString() = " + request.getQueryString());
        // request.isSecure() = false (https 여부)
        System.out.println("request.isSecure() = " + request.isSecure());
    }

    private void printRequestHeaderValue(HttpServletRequest request, String headerName) {
        System.out.println("request.getHeader(\"" + headerName + "\") = " + request.getHeader(headerName));
    }
}
