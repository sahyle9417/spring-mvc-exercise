package springmvc.springmvcexercise.springmvc.v1;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SpringMemberFormControllerV1 {

    @RequestMapping("/springmvc/v1/members/new-form")
    public ModelAndView handleRequest() {
        // resolved to /WEB-INF/views/new-form.jsp using application.properties
        return new ModelAndView("new-form");
    }
}
