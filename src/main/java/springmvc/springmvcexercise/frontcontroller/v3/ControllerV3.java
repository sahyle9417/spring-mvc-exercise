package springmvc.springmvcexercise.frontcontroller.v3;

import springmvc.springmvcexercise.frontcontroller.ModelView;

import java.util.Map;

public interface ControllerV3 {

    ModelView process(Map<String, String> paramMap);
}
