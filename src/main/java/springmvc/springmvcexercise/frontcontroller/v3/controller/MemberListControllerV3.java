package springmvc.springmvcexercise.frontcontroller.v3.controller;

import springmvc.springmvcexercise.domain.member.Member;
import springmvc.springmvcexercise.domain.member.MemberRepository;
import springmvc.springmvcexercise.frontcontroller.ModelView;
import springmvc.springmvcexercise.frontcontroller.MyView;
import springmvc.springmvcexercise.frontcontroller.v3.ControllerV3;

import java.util.List;
import java.util.Map;

public class MemberListControllerV3 implements ControllerV3 {

    private MemberRepository memberRepository = MemberRepository.getInstance();

    @Override
    public ModelView process(Map<String, String> paramMap) {
        List<Member> members = memberRepository.findAll();
        ModelView modelView = new ModelView("members");
        modelView.getModel().put("members", members);
        return modelView;
    }
}
